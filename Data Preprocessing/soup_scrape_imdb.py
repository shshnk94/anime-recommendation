from urllib.request import urlopen
from bs4 import BeautifulSoup as bs

def main():

    #URL in imdb.com with search keyword "anime"
    url = "http://www.imdb.com/search/keyword?keywords=anime"

    while True:
        #Create a BeautifulSoup object to parse the web-page containing the anime list
        soup = bs(urlopen(url).read(), "html.parser")

        #Each anime details are put into tiles (<div/> tags) with names "lister-item mode-detail". 
        #We extract the references to each such tile and iterate across to scrape the details off of each anime.

        item_containers = soup.findAll("div", {"class" : "lister-item mode-detail"})
        for item in item_containers:

            content = item.find("div", {"class" : "lister-item-content"})
            title = content.a.get_text()

            #Calling get_text() when the object is 'NoneType' raises an AttributeError
            #Similarly, indexing a 'NoneType' object for some content results in TypeError
            try:
                genre = content.find("span", {"class" : "genre"}).get_text().strip().replace(",", "|")
            except AttributeError:
                genre = ""

            try:
                rate = content.strong.get_text()
            except AttributeError:
                rate = ""

            try:
                best_rate = content.find("meta", {"itemprop" : "bestRating"})['content']
            except TypeError:
                best_rate = ""

            try:
                votes = content.find("meta", {"itemprop" : "ratingCount"})['content']
            except TypeError:
                votes = ""

            print(title + "," + genre + "," + rate + "," + best_rate + "," + votes)

        try:
            next_page = soup.find("a", {"class" : "lister-page-next next-page"})['href']
        except TypeError:
            #Raises a TypeError exception if there are no further "next" links to be crawled
            return

        url = str(url.split('?')[0] + next_page).split('#')[0]

main()
