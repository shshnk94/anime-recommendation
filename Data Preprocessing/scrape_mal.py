import myanimelist.session as mal
from myanimelist.anime import MalformedAnimePageError
import csv
import time

def main():
    data = []
    session = mal.Session()

    #List all the features to be extracted from an anime
    features = [
            '_title',
            '_aired',
            '_duration',
            '_episodes',
            '_producers',
            '_rating',
            '_type',
            #'_staff',
            #'_voice_actors',
            #'_characters',
            '_favorites',
            '_genres',
            '_members',
            '_popularity',
            '_rank',
            '_score',
            '_status',
            '_related']

    anime_id = 36425
    while True:
        record = [str(anime_id)]
        anime = session.anime(anime_id)
        try:
            print(anime_id)
            anime.load()
            print("Anime id - " + str(anime_id) + " : " + str(anime.title))
            for i in features:
                if anime.__dict__[i] is None:
                    record.append('')
                elif i == '_genres':
                    genre = ""
                    for item in anime.__dict__[i]:
                        genre = genre + "|" + item.name
                    record.append(genre.strip('|'))
                elif i == '_aired':
                    try:
                        aired = str(anime.__dict__[i][0]) + "|" + str(anime.__dict__[i][1])
                    except IndexError:
                        aired = str(anime.__dict__[i][0]) + "|"
                    record.append(aired)
                elif i == '_producers':
                    producers = ""
                    for prod in anime.__dict__[i]:
                        producers += "|" + prod.name
                    record.append(producers.strip('|'))
                elif i == '_score':
                    record.append(str(anime.__dict__[i][0]))
                    record.append(str(anime.__dict__[i][1]))
                elif i == '_related':
                    related = ""
                    try:
                        for item in anime.__dict__[i]['Alternative version']:
                            related += "|" + str(item.id)
                    except KeyError:
                        pass
                    try:
                        for item in anime.__dict__[i]['Spin-off']:
                            related += "|" + str(item.id)
                    except KeyError:
                        pass
                    try:
                        for item in anime.__dict__[i]['Sequel']:
                            related += "|" + str(item.id)
                    except KeyError:
                        pass
                    try:
                        for item in anime.__dict__[i]['Prequel']:
                            related += "|" + str(item.id)
                    except KeyError:
                        pass

                    record.append(related.strip('|'))

                elif i == '_characters':
                    character = ""
                    for item in anime.__dict__[i]:
                        for actor in anime.__dict__[i][item]['voice_actors']:
                            if anime.__dict__[i][item]['voice_actors'][actor] == 'Japanese':
                                character += "|" + actor.name
                    record.append(character)
                else:
                    record.append(str(anime.__dict__[i]))
                    
        except AttributeError as e:
            print(e)
            anime_id -= 1
            time.sleep(20)

        except (IndexError, MalformedAnimePageError) as e:
            pass

        else:
            data.append(record)
            with open("data.csv", "wt") as f:
                writer = csv.writer(f, delimiter = ',')
                for item in data:
                    writer.writerow(item)

        anime_id += 1

main()
