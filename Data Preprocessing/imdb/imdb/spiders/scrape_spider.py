import scrapy

class AnimeSpider(scrapy.Spider):
    name = "anime"
    url = ""
    data = []
    
    #Initially, the base URL is passed to start_requests() which calls scrapy.Request(url, callback)
    #Request() and parse() are executed asynchronously and the order in which the URLs are further crawled 
    #depends on the scrapy scheduler.

    def start_requests(self):
        self.url = "http://www.imdb.com/search/keyword?keywords=anime"

        yield scrapy.Request(url = self.url, callback = self.parse)

    def parse(self, response):

        #Each anime cell(item) is extracted and further data is scraped out of it.
        item_containers = response.xpath('//div[@class="lister-item mode-detail"]')
        for item in item_containers:
            content = item.xpath('.//div[@class="lister-item-content"]')

            title = content.xpath('.//a/text()').extract_first()

            #Certain anime attributes which are not yet updated in the website raise IndexError which are handled.
            try:
                genre = str(content.xpath('.//span[@class="genre"]/text()').extract()[0]).strip().replace(',', '|')
            except IndexError:
                genre = ""

            try:
                rate = content.xpath('.//strong/text()').extract()[0]
            except IndexError:
                rate = ""

            try:
                best_rate = content.xpath('.//meta[@itemprop="bestRating"]/@content').extract()[0]
            except IndexError:
                best_rate = ""

            try:
                votes = content.xpath('.//meta[@itemprop="ratingCount"]/@content').extract()[0]
            except IndexError:
                votes = ""

            self.data.append(str(title + "," + genre + "," + rate + "," + best_rate + "," + votes))

        #URL of the next page to be crawled is constructed by scraping off the "Next" hyperlink in the bottom of the page.
        path = response.css('a.lister-page-next::attr(href)').extract_first()
        try:
            yield scrapy.Request(url = str(self.url.split('?')[0] + path.split('#')[0] ), callback = self.parse)
        except AttributeError:
            for i in self.data:
                yield {'title,genre,rate,best_rate,votes' : i}


